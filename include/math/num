// -*- mode: c++ -*-
#pragma once

#include <cmath>

/**
 * @ingroup math
 * @brief Generic numeric constants and math
 */
namespace num {
  /**
   * @brief The @f$\pi@f$ constant
   */
  template<typename T>
  constexpr T pi() {
    return 3.14159265358979323846;
  }

  /**
   * @brief The @f$e@f$ constant
   */
  template<typename T>
  constexpr T e() {
    return 2.71828182845904523536;
  }

  /**
   * @brief The __golden ratio__ constant
   */
  template<typename T>
  constexpr T phi() {
    return 1.61803398874989484820;
  }
}
