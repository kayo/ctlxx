// -*- mode: c++ -*-
#pragma once
#include "../type/vec"
#include "./osc"

/**
 * @ingroup transforms
 * @brief N-phase sine-wave modulation
 *
 * Generic classic N-phase sine-wave modulator implementation.
 */
namespace swm {
  /**
   * @brief Modulator parameters
   */
  template<typename P>
  using param = osc::param<P>;

  /**
   * @brief Modulator state
   */
  template<typename P>
  using state = osc::state<P>;

  /**
   * @brief Modulation using ½π units for phase
   */
  namespace hpi {
    /**
     * @brief Apply 1-phase modulation
     */
    template<typename P, typename S>
    void apply(state<P> &state, const param<P> &param, S hpi_sin, vec1<decltype(hpi_sin(P()))> &a) {
      a[0] = hpi_sin(state.phase);

      osc::hpi::apply(state, param);
    }

    /**
     * @brief Apply 2-phase modulation
     *
     * Output phase offsets: [0 π]
     */
    template<typename P, typename S>
    void apply(state<P> &state, const param<P> &param, S hpi_sin, vec2<decltype(hpi_sin(P()))> &ab) {
      ab[0] = hpi_sin(state.phase);
      ab[1] = -ab[0]; // hpi_sin(state.phase + P(2.0));

      osc::hpi::apply(state, param);
    }

    /**
     * @brief Apply 3-phase modulation
     *
     * Output phase offsets: [-⅔π 0 ⅔π]
     */
    template<typename P, typename S>
    void apply(state<P> &state, const param<P> &param, S hpi_sin, vec3<decltype(hpi_sin(P()))> &abc) {
      abc[0] = hpi_sin(P(state.phase - P(4.0 / 3.0)));
      abc[1] = hpi_sin(state.phase);
      abc[2] = -(abc[0] + abc[1]); // hpi_sin(P(state.phase + P(4.0 / 3.0)));

      osc::hpi::apply(state, param);
    }
  }
}
