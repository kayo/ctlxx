// -*- mode: c++ -*-
#pragma once
#include "../type/vec"
#include <cmath>

/**
 * @ingroup transforms
 * @brief α-β transformation
 *
 * The implementation of α-β transformations which also known as the Clarke transformations.
 *
 * See also [Alpha-Beta transformation](https://en.wikipedia.org/wiki/Alpha-beta_transformation)
 */
namespace ab {
  /**
   * @brief Transform ABC to α-β
   *
   * The direct Clarke transformation
   *
   * @f$α = A@f$, @f$β = \frac{A + 2 B}{\sqrt{3}}@f$
   */
  template<typename O, typename I>
  constexpr void from_abc(vec2<O> &ab, const vec2<I> &abc) {
    /* α = a */
    ab.a = abc.a;

    /* β = (a + 2 * b) / sqrt(3) */
    ab.b = O(O(abc.a + abc.b) + abc.b) * O(1.0 / sqrt(3.0));
  }

  /**
   * @brief Transform ABC to α-β
   *
   * The direct Clarke transformation
   *
   * @f$α = A@f$, @f$β = \frac{A + 2 B}{\sqrt{3}}@f$
   */
  template<typename O, typename I>
  inline constexpr vec2<O> from_abc(const vec2<I> &abc) {
    vec2<O> ab;
    from_abc<O, I>(ab, abc);
    return ab;
  }

  /**
   * @brief Transform ABC to α-β
   *
   * The direct Clarke transformation
   *
   * @f$α = A@f$, @f$β = \frac{A + 2 B}{\sqrt{3}}@f$
   */
  template<typename O, typename I>
  constexpr void from_abc(vec2<O> &ab, const vec3<I> &abc) {
    from_abc<O, I>(ab, (const vec2<I>&)abc);
  }

  /**
   * @brief Transform ABC to α-β
   *
   * The direct Clarke transformation
   *
   * @f$α = A@f$, @f$β = \frac{A + 2 B}{\sqrt{3}}@f$
   */
  template<typename O, typename I>
  inline constexpr vec2<O> from_abc(const vec3<I> &abc) {
    vec2<O> ab;
    from_abc<O, I>(ab, abc);
    return ab;
  }

  /**
   * @brief Transform α-β to ABC
   *
   * The inverted Clarke transformation
   *
   * @f$A = α@f$, @f$B = \frac{- α + \sqrt{3} β}{2}@f$, @f$C = \frac{- α -\sqrt{3} β}{2}@f$
   *
   */
  template<typename O, typename I>
  constexpr void into_abc(vec3<O> &abc, const vec2<I> &ab) {
    // a = α
    abc.a = ab.a;

    // t1 = -α / 2
    O t1 = - ab.a / 2;

    // t2 = β * sqrt(3) / 2
    O t2 = ab.b * O(sqrt(3.0) / 2.0);

    // b = t1 + t2
    abc.b = t1 + t2;

    // c = t1 - t2
    abc.c = t1 - t2;
  }

  /**
   * @brief Transform α-β to ABC
   *
   * The inverted Clarke transformation
   *
   * @f$A = α@f$, @f$B = \frac{- α + \sqrt{3} β}{2}@f$, @f$C = \frac{- α -\sqrt{3} β}{2}@f$
   */
  template<typename O, typename I>
  inline constexpr vec3<O> into_abc(const vec2<I> &ab) {
    vec3<O> abc;
    into_abc<O, I>(abc, ab);
    return abc;
  }
}
