// -*- mode: c++ -*-
#pragma once
#include <math/trig>

/**
 * @ingroup transforms
 * @brief Basic oscillator
 *
 * Generic implementation of basic oscillator.
 */
namespace osc {
  /**
   * @brief Oscillator parameters
   */
  template<typename P>
  struct param {
    /// Phase increment for single step
    P delta;
  };

  /**
   * @brief Oscillator state
   */
  template<typename P>
  struct state {
    /// Current phase of modulator
    P phase;

    /// Init state with zero phase
    state(): phase(P(0.0)) {}
  };

  /**
   * @brief Operations in ½π units
   */
  namespace hpi {
    /**
     * @brief Init oscillator parameters using frequency
     */
    template<typename P, typename F, typename T>
    inline constexpr param<P> from_frequency(const F &frequency, const T &period) {
      P delta = cyc2hpi<P>(frequency * period);
      return { delta };
    }

    /**
     * @brief Clamp phase to range -2Pi ... 2Pi
     */
    template<typename P>
    constexpr P clamp(const P &phase) {
      return fmod(phase, P(4.0));
    }

    /**
     * @brief Init oscillator state using initial phase
     */
    template<typename P>
    constexpr state<P> init(const P &phase) {
      return { clamp(phase) };
    }

    /**
     * @brief Set oscillator phase
     */
    template<typename P>
    void set(state<P> &state, const P &phase) {
      state.phase = clamp(phase);
    }

    /**
     * @brief Apply oscillator step
     */
    template<typename P>
    void apply(state<P> &state, const param<P> &param) {
      set(state, P(state.phase + param.delta));
    }
  }
}
