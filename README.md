## uControl

Generic control library for embedded devices which purposes to developing control systems and hardware monitors.

### Overview

This library consist of several independent units, such as filters, controllers and convertors.
Also it includes some useful utilities, such as clampers and scalers.

All of that can operate with both floating and fixed point values.

The filters and controllers can be configured in a human-friendly way without using obscure artifical coefficients.

### Filters

* [EMA](https://en.wikipedia.org/wiki/Moving_average#Exponential_moving_average) (exponential moving average)
* [PT1](https://de.wikipedia.org/wiki/PT1-Glied) (first order proportional transmission behavior) filter
* [LQE](https://en.wikipedia.org/wiki/Kalman_filter) (Kalman) filter
* [FIR](https://en.wikipedia.org/wiki/Finite_impulse_response) (finite impulse response) filters

### Regulators

* [PID](https://en.wikipedia.org/wiki/PID_controller) (proportional integral derivative) controller

### Transformations

* [αβ](https://en.wikipedia.org/wiki/Alpha-beta_transformation) (Clarke) transformation
* [DQZ](https://en.wikipedia.org/wiki/Direct-quadrature-zero_transformation) (Park) transformation
* 3-phase sine-wave modulation
* [SVM](https://en.wikipedia.org/wiki/Space_vector_modulation) (space-vector) modulation
* Phase-shift correction
