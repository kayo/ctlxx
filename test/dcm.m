# -*- mode: octave -*-

data = get_data("dcm", "DELTA_TIME", 0.00001, "LIMIT_TIME", 0.7);

t = data(:,1);
U = data(:,2);
T = data(:,3);
w = data(:,4);
I = data(:,5);

subplot(6,1,1);
plot(t, U);
#xlabel("t, S");
ylabel("U, V");
#title("Supply voltage");

subplot(6,1,2);
plot(t, T);
#xlabel("t, S");
ylabel("T, N⋅m");
#title("Load torque");

subplot(3,1,2);
plot(t, w*30/pi);
#xlabel("t, S");
ylabel("ω, RPM");
#title("Rotation speed");

subplot(3,1,3);
plot(t, I);
xlabel("t, S");
ylabel("I, A");
#title("Supply current");
