#include <iostream>

#include <type/vec>
#include <util/lens>
#include <type/fix>
#include "test.hxx"

template<typename T>
struct S {
  T value;
  int flags;
};

template<typename T>
std::ostream &operator << (std::ostream &out, const S<T> &self) {
  return out << "{" << self.value << "," << self.flags << "}";
}

tests {
  {
    vec<1, float> v = { 1.0 };
    assert_eq(v.len, 1u);
    assert_eq(sizeof(v), sizeof(float));

    assert_eq(v[0], 1.0);

    assert_eq((v + 1.0)[0], 2.0);
  }

  {
    vec2<float> v = { 1.0, 2.0 };
    assert_eq(v.len, 2u);
    assert_eq(sizeof(v), sizeof(float) * 2);

    assert_eq(v[0], 1.0);
    assert_eq(v[1], 2.0);

    assert_eq(v.x, 1.0);
    assert_eq(v.y, 2.0);

    assert_eq(v.a, 1.0);
    assert_eq(v.b, 2.0);

    assert_eq(v.d, 1.0);
    assert_eq(v.q, 2.0);

    assert_eq((v + 1.0)[0], 2.0);
    assert_eq((v + 1.0)[1], 3.0);

    assert_eq((v - 1.0)[0], 0.0);
    assert_eq((v - 1.0)[1], 1.0);

    vec2<float> v2 = { -2.0, -1.0 };

    assert_eq((v + v2)[0], -1.0);
  }

  {
    vec3<float> v = { 1.0, 2.0, 3.0 };
    assert_eq(v.len, 3u);
    assert_eq(sizeof(v), sizeof(float) * 3);

    assert_eq(v[0], 1.0);
    assert_eq(v[1], 2.0);
    assert_eq(v[2], 3.0);

    assert_eq(v.x, 1.0);
    assert_eq(v.y, 2.0);
    assert_eq(v.z, 3.0);

    assert_eq(v.a, 1.0);
    assert_eq(v.b, 2.0);
    assert_eq(v.c, 3.0);

    assert_eq(v.d, 1.0);
    assert_eq(v.q, 2.0);

    assert_eq((v + 1.0)[0], 2.0);
    assert_eq((v + 1.0)[1], 3.0);

    assert_eq((v - 1.0)[0], 0.0);

    vec3<float> v2 = { -2.0, -1.0 };

    assert_eq((v + v2)[0], -1.0);
    assert_eq((v + v2)[1], 1.0);
    assert_eq((v - v2)[0], 3.0);
    assert_eq((v - v2)[1], 3.0);
  }

  {
    vec4<float> v = { 1.0, 2.0, 3.0, 4.0 };
    assert_eq(v.len, 4u);
    assert_eq(sizeof(v), sizeof(float) * 4);
  }

  {
    static const auto v =
      gen_vec<10>([](len_t i, len_t l) {
                    return i * l;
                  });

    assert_eq(sizeof(v), sizeof(len_t) * 10);
    assert_eq(v[0], 0u);
    assert_eq(v[1], 10u);
    assert_eq(v[2], 20u);
    assert_eq(v[5], 50u);
    assert_eq(v[9], 90u);
  }

  {
    static int raw[4] = { 1, 2, 3, 4 };
    auto v = vec<4, int>::wrap(raw);

    assert_eq(v[0], 1);
    assert_eq(v[1], 2);
    assert_eq(v[2], 3);
    assert_eq(v[3], 4);
  }

  {
    static vec<7, int> v = { 1, 2, 3, 4, 5, 6, 7};
    auto s = v.template slice<2, 3>();

    assert_eq(s.len, 3u);
    assert_eq(s[0], 3);
    assert_eq(s[1], 4);
    assert_eq(s[2], 5);
  }

  {
    static vec<5, int> v = { 4, 2, 1, 5, 3 };

    const auto sort = bubble_sort<5, int>();

    sort(v);

    assert_eq(v[0], 1);
    assert_eq(v[1], 2);
    assert_eq(v[2], 3);
    assert_eq(v[3], 4);
    assert_eq(v[4], 5);
  }

  {
    using val = bfix<15,-6>;
    static vec<5, val> v = { -4.2, 2.6, -1.1, 5.3, 0.1 };

    const auto pick = lens::self<val>;
    const auto sort = bubble_sort<5, val>(pick);

    sort(v);

    assert_eq(v[0], val(-4.2));
    assert_eq(v[1], val(-1.1));
    assert_eq(v[2], val(0.1));
    assert_eq(v[3], val(2.6));
    assert_eq(v[4], val(5.3));
  }

  {
    using val = bfix<15,-8>;
    static vec<5, S<val>> v = { S<val>{-4.2, 0}, {2.6, 1}, {-1.1, 0}, {5.3, 3}, {0.1, 1} };

    const auto pick_value = lens::field<S<val>>(&S<val>::value);
    const auto sort = bubble_sort<5, S<val>>(pick_value, pick_value);

    sort(v);

    assert_eq(v[0].value, val(-4.2));
    assert_eq(v[0].flags, 0);
    assert_eq(v[1].value, val(-1.1));
    assert_eq(v[1].flags, 1);
    assert_eq(v[2].value, val(0.1));
    assert_eq(v[2].flags, 0);
    assert_eq(v[3].value, val(2.6));
    assert_eq(v[3].flags, 3);
    assert_eq(v[4].value, val(5.3));
    assert_eq(v[4].flags, 1);
    }

  {
    using val = int;
    static vec<5, S<val>> v = { S<val>{2, 1}, {-4, 0}, {-1, 0}, {5, 3}, {0, 1} };

    const auto pick_value = lens::field<S<val>>(&S<val>::value);
    const auto pick_elem = lens::self<S<val>>;
    const auto sort = bubble_sort<5, S<val>>(pick_value, pick_elem);

    sort(v);

    assert_eq(v[0].value, val(-4));
    assert_eq(v[0].flags, 0);
    assert_eq(v[1].value, val(-1));
    assert_eq(v[1].flags, 0);
    assert_eq(v[2].value, val(0));
    assert_eq(v[2].flags, 1);
    assert_eq(v[3].value, val(2));
    assert_eq(v[3].flags, 1);
    assert_eq(v[4].value, val(5));
    assert_eq(v[4].flags, 3);
  }

  {
    using val = bfix<16,-10>;
    static vec<5, S<val>> v = { S<val>{2.6, 1}, {val(-4.2), 0}, {val(-1.1), 0}, {val(5.3), 3}, {val(0.1), 1} };

    const auto pick_value = lens::field<S<val>>(&S<val>::value);
    const auto pick_elem = lens::self<S<val>>;
    const auto sort = bubble_sort<5, S<val>>(pick_value, pick_elem);

    sort(v);

    assert_eq(v[0].value, val(-4.2));
    assert_eq(v[0].flags, 0);
    assert_eq(v[1].value, val(-1.1));
    assert_eq(v[1].flags, 0);
    assert_eq(v[2].value, val(0.1));
    assert_eq(v[2].flags, 1);
    assert_eq(v[3].value, val(2.6));
    assert_eq(v[3].flags, 1);
    assert_eq(v[4].value, val(5.3));
    assert_eq(v[4].flags, 3);
  }

  {
    static vec<5, int> v = { 4, 2, 1, 5, 3 };

    const auto sort = comb_sort<float, 5, int>();

    sort(v);

    assert_eq(v[0], 1);
    assert_eq(v[1], 2);
    assert_eq(v[2], 3);
    assert_eq(v[3], 4);
    assert_eq(v[4], 5);
  }

  {
    using val = bfix<15,-6>;
    static vec<5, val> v = { -4.2, 2.6, -1.1, 5.3, 0.1 };

    const auto sort = comb_sort<bfix<24,-8>, 5, val>(lens::self<val>, lens::self<val>);

    sort(v);

    assert_eq(v[0], val(-4.2));
    assert_eq(v[1], val(-1.1));
    assert_eq(v[2], val(0.1));
    assert_eq(v[3], val(2.6));
    assert_eq(v[4], val(5.3));
  }
}
