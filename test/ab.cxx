#include <math/trig>
#include <trans/swm>
#include <trans/ab>
#include "test.hxx"

#ifndef DELTA_TIME
#define DELTA_TIME 1.0
#endif

#ifndef LIMIT_TIME
#define LIMIT_TIME 100.0
#endif

#define HPI_TRIG

tests {
  auto osc_param = osc::hpi::from_frequency<float>(50.0, DELTA_TIME);
  auto osc_state = osc::state<float>();

  vec3<float> osc;
  vec2<float> ab;
  vec3<float> abc;

#ifdef HPI_TRIG
  const auto sin = hpi::sin3<float, float, float>;
#else
  //const auto sin = sin3<float, float, float>;
  const auto sin = sinf;
#endif

  for (float t = 0.0; t <= LIMIT_TIME; t += DELTA_TIME) {
#ifdef HPI_TRIG
    swm::hpi::apply(osc_state, osc_param, sin, osc);
#else
    swm::apply(osc_state, osc_param, sin, osc);
#endif

    ab::from_abc(ab, osc);
    ab::into_abc(abc, ab);

#if PLOT
    std::cout << t << ' ' << osc << ' ' << ab << ' ' << abc << std::endl;
#endif
  }
}
