# -*- mode: octave -*-

data = get_data("swm", "DELTA_TIME", 0.0001, "LIMIT_TIME", 1/50, "PHASES", 3);

t = data(:,1);
a = data(:,2);
b = data(:,3);
c = data(:,4);
v = max(a, max(b, c)) .- min(a, min(b, c));

plot(t, a, "-;A;",
     t, b, "-;B;",
     t, c, "-;C;",
     t, v, "-;P;");
xlabel("t, S");
ylabel("U, V");
title("Classic 3-phase sine-wave modulation");
