#include <model/dcm>
#include "test.hxx"

#ifndef DELTA_TIME
#define DELTA_TIME 0.0001
#endif

#ifndef LIMIT_TIME
#define LIMIT_TIME 0.6
#endif

tests {
  const dcm::params<float, float, float, float>
    // BaneBots RS-775 18V DC brush motor
    // Rr=124mOhm, Lr=42uH (50uH?), Fs=8.5mWb, Jr=8.71e-6
    motor(124e-3, 42.0e-6, 8.5e-3, 8.71e-6);
  const dcm::param<float, float, float, float> param(motor, float(DELTA_TIME));
  dcm::state<float, float, float, float> state;

  for (float t = 0.0; t < LIMIT_TIME; t += DELTA_TIME) {
    float Ur = t >= 0.15 && t < 0.35 ? 13.56 : t < 0.5 ? 12.0 : 0.0;
    float Tl = t >= 0.1 && t < 0.3 ? 124e-3 : 13.6e-3;

    dcm::apply(state, param, Ur, Tl);
#ifdef PLOT
    std::cout << t << ' ' << Ur << ' ' << Tl << ' ' << state.wr << ' ' << state.Ir << std::endl;
#endif
  }
}
