#include <type/fix>
#include <filt/ema>

#define EPS 0.000001
#include "test.hxx"

tests {
  { // from alpha
    static const auto param = ema::param<float>::from_alpha(0.6);

    assert_eqe(param.alpha, 0.6);
    assert_eqe(param.c11y_alpha, 0.4);
  }

  { // with gain
    static const auto param = ema::param<float>::from_alpha(0.6) * 1.2;

    assert_eqe(param.alpha, 0.72);
    assert_eqe(param.c11y_alpha, 0.48);
  }

  { // from samples
    static const auto param = ema::param<float>::from_samples(2.0);

    assert_eqe(param.alpha, 0.6666667);
    assert_eqe(param.c11y_alpha, 0.3333333);

    static ema::state<float> state;

    assert_eqe(ema::apply(state, param, 1.0), 0.6666667);
    assert_eqe(ema::apply(state, param, 1.0), 0.8888889);
  }

  { // from time
    static const auto param = ema::param<float>::from_time(4.0, 0.1);

    assert_eqe(param.alpha, 0.0487805);
    assert_eqe(param.c11y_alpha, 0.951219);
  }

  { // from pt1
    static const auto param = ema::param<float>::from_pt1(4.0, 0.1);

    assert_eqe(param.alpha, 0.0243902);
    assert_eqe(param.c11y_alpha, 0.97561);
  }

  { // from time to fixed
    static const ema::param<bfix<16,-15>> param = ema::param<float>::from_time(4.0, 0.1);

    assert_eq(param.alpha, (bfix<16,-15>(0.0487805)));
    assert_eq(param.c11y_alpha, (bfix<16,-15>(0.951219)));
  }
}
