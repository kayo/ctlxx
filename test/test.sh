#!/bin/sh

src=$1
bin=$src.bin
shift
args=
for arg in "$@"; do args="$args -D$arg"; done

g++ -I../include -I./ -std=c++14 -o $bin $src $args
./$bin
rm -f $bin
