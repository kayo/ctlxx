# -*- mode: octave -*-

data = get_data("ab", "DELTA_TIME", 0.0001, "LIMIT_TIME", 1/50, "PHASES", 3);

t = data(:,1);
a1 = data(:,2);
b1 = data(:,3);
c1 = data(:,4);
a2 = data(:,5);
b2 = data(:,6);
a3 = data(:,7);
b3 = data(:,8);
c3 = data(:,9);

subplot(3,1,1);
plot(t, a1, "-;A;",
     t, b1, "-;B;",
     t, c1, "-;C;");
xlabel("t, S");
ylabel("U, V");
title("Source signal");

subplot(3,1,2);
plot(t, a2, "-;α;",
     t, b2, "-;β;");
xlabel("t, S");
ylabel("U, V");
title("Direct Clarke transformation");

subplot(3,1,3);
plot(t, a3, "-;A;",
     t, b3, "-;B;",
     t, c3, "-;C;");
xlabel("t, S");
ylabel("U, V");
title("Inverted Clarke transformation");
