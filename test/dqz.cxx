#include <math/trig>
#include <trans/swm>
#include <trans/ab>
#include <trans/dqz>
#include "test.hxx"

#ifndef DELTA_TIME
#define DELTA_TIME 1.0
#endif

#ifndef LIMIT_TIME
#define LIMIT_TIME 100.0
#endif

tests {
  auto osc_param = osc::hpi::from_frequency<float>(50.0, DELTA_TIME);
  auto osc_state = osc::state<float>();

  vec3<float> osc;
  vec2<float> ab;
  vec2<float> dq;
  vec2<float> iab;

  const auto sin = hpi::sin3<float, float, float>;

  for (float t = 0.0; t <= LIMIT_TIME; t += DELTA_TIME) {
    swm::hpi::apply(osc_state, osc_param, sin, osc);

    float a = t * 50.0 * 4;

    ab::from_abc(ab, osc);
    dqz::hpi::into_rot(dq, ab, sin, a);
    dqz::hpi::into_stat(iab, dq, sin, a);

#if PLOT
    std::cout << t << ' ' << ab << ' ' << dq << ' ' << iab << std::endl;
#endif
  }
}
