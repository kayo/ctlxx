#include "test.hxx"
#include <type/fix>
#include <util/lt>
#include <cmath>

static const lt1<32, float, float, float>
float_sin(0.0, M_PI/2.0, [](float x) { return sin(x); });

static const lt1<32, bfix<16, -13>, bfix<16, -15>, bfix<12, -8>>
fixed_sin(0.0, M_PI/2.0, [](bfix<16, -12> x) { return sin(float(x)); });

tests {
#define EPS 1e-5

  assert_eqe(float_sin(0.0), 0.0);
  assert_eqe(float_sin(M_PI/6.0), 0.499856);
  assert_eqe(float_sin(M_PI/2.0), 1.0);

  assert_eqe(float(fixed_sin(0.0)), 0.0);
  assert_eqe(float(fixed_sin(M_PI/6.0)), 0.499634);
  assert_eqe(float(fixed_sin(M_PI/2.0)), 0.999939);
}
