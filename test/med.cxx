#include <type/fix>
#include <filt/med>
#include <filt/avg>
#include "test.hxx"

tests {
  {
    static vec<7, int> data = { 7, 1, 5, 3, 2, -4, 2 };

    const auto med_filter = med::make<3, 7, int>();

    auto out = med_filter(data);

    assert_eq(out.len, 3u);
    assert_eq(out[0], 2);
    assert_eq(out[1], 2);
    assert_eq(out[2], 3);

    const auto avg_filter = avg::make<int, 3, int>();

    assert_eq(avg_filter(out), 2);
  }

  {
    static vec<7, int> data = { 7, 1, 5, 3, 2, -4, 2 };

    auto out = med::apply<3>(data);

    assert_eq(out.len, 3u);
    assert_eq(out[0], 2);
    assert_eq(out[1], 2);
    assert_eq(out[2], 3);

    assert_eq(avg::apply<int>(out), 2);
  }

  {
    static vec<7, int> data = { 7, 1, 5, 3, 2, -4, 2 };

    const auto sort = comb_sort<bfix<15,6>, 7, int>();
    const auto med_filter = med::make<4, 7, int>(sort);

    auto out = med_filter(data);

    assert_eq(out.len, 4u);
    assert_eq(out[0], 1);
    assert_eq(out[1], 2);
    assert_eq(out[2], 2);
    assert_eq(out[3], 3);

    const auto avg_filter = avg::make<int, 4, int>(lens::self<int>);

    assert_eq(avg_filter(out), 2);
  }

  {
    static vec<7, int> data = { 7, 1, 5, 3, 2, -4, 2 };

    const auto sort = comb_sort<bfix<15,6>, 7, int>();

    auto out = med::apply<4>(data, sort);

    assert_eq(out.len, 4u);
    assert_eq(out[0], 1);
    assert_eq(out[1], 2);
    assert_eq(out[2], 2);
    assert_eq(out[3], 3);

    assert_eq(avg::apply<int>(out, lens::self<int>), 2);
  }
}
