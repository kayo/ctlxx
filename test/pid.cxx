#include <regul/pid>
#include <model/htr>
#include "test.hxx"

#ifndef DELTA_TIME
#define DELTA_TIME 1.0
#endif

#ifndef LIMIT_TIME
#define LIMIT_TIME 100.0
#endif

#ifndef AMB_TEMP
#define AMB_TEMP 25.0
#endif

#ifndef INI_TEMP
#define INI_TEMP AMB_TEMP
#endif

tests {
  assert_eq(sizeof(pid::param<>), 1u); // initial
  assert_eq(sizeof(pid::param<float>), 1u*sizeof(float)); // P
  assert_eq(sizeof(pid::param<float, void, float>), 2u*sizeof(float)); // PD
  assert_eq(sizeof(pid::param<float, float>), 2u*sizeof(float)); // PI
  assert_eq(sizeof(pid::param<float, float, void, float>), 3u*sizeof(float)); // PI + Ei_lim
  assert_eq(sizeof(pid::param<float, float, float>), 3u*sizeof(float)); // PID
  assert_eq(sizeof(pid::param<float, float, float, float>), 4u*sizeof(float)); // PID + Ei_lim

  assert_eq(sizeof(pid::state<>), 1u); // proportional
  assert_eq(sizeof(pid::state<float>), 1u*sizeof(float)); // PD
  assert_eq(sizeof(pid::state<float, float>), 2u*sizeof(float)); // PID
  assert_eq(sizeof(pid::state<void, float>), 1u*sizeof(float)); // PI

  const auto htr_param = htr::param<float>(990.0f, 6.75e-3f, 8.4f, float(AMB_TEMP), float(DELTA_TIME));
  htr::state<float> htr_state(float(INI_TEMP));

#ifdef CONST_POWER
  for (float t = 0.0; t <= LIMIT_TIME; t += DELTA_TIME) {
    float tmp = htr::apply(htr_state, htr_param, CONST_POWER);
#ifdef PLOT
    std::cout << t << ' ' << tmp << ' ' << CONST_POWER << std::endl;
#endif
  }
#endif

#if !defined(PID_P) && !defined(PID_I) && !defined(PID_D)
#define PID_P
#define PID_I
#define PID_D
#endif

#ifdef PID_TARGET
  static const auto param = pid::param<>()
    .with_Kp<float>(0.5)
#if defined(PID_I)
    .with_Ti<float, float, float>(120.0, DELTA_TIME)
#endif
#if defined(PID_D)
    .with_Td<float, float, float>(8.0, DELTA_TIME)
#endif
    ;
#if defined(PID_I) && defined(PID_D)
  static auto state = pid::pid<float, float>();
#endif
#if defined(PID_I) && !defined(PID_D)
  static auto state = pid::pi<float>();
#endif
#if !defined(PID_I) && defined(PID_D)
  static auto state = pid::pd<float>();
#endif
#if !defined(PID_I) && !defined(PID_D)
  static pid::state<> state;
#endif
  float tmp = htr_state.T0;

#ifndef PLOT
  std::cout << "sizeof(param)=" << sizeof(param) << std::endl;
  std::cout << "sizeof(state)=" << sizeof(state) << std::endl;
#endif

  for (float t = 0.0; t <= LIMIT_TIME; t += DELTA_TIME) {
    float pwr = pid::apply<float>(state, param, adj::error(tmp, float(PID_TARGET)));
    pwr = adj::clamp(pwr, 0.0f, 40.0f);
    tmp = htr::apply(htr_state, htr_param, pwr);
#ifdef PLOT
    std::cout << t << ' ' << tmp << ' ' << pwr << ' ' << PID_TARGET << std::endl;
#endif
  }
#endif
}
