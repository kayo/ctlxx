#include <type/opt>
#include "test.hxx"

tests {
  assert_eq(sizeof(opt<float>), sizeof(float) * 2);
  assert_eq(sizeof(opt<double>), sizeof(double) * 2);

  {
    static const auto v = opt<int>();

    assert_eq(v.is_some(), false);
    assert_eq(v.is_none(), true);

    assert_eq(v.and_(9).is_some(), false);
    assert_eq(v.or_(9).is_some(), true);
    assert_eq(v.or_(9).unwrap(), 9);

    assert_eq(v.then([](const opt<int> &i) {
                      assert(true);
                      assert_eq(i.is_some(), false);
                      return 1;
                    }), 1);

    assert_eq(v.then([](const opt<int> &i) {
                       assert(true);
                       assert_eq(i.is_some(), false);
                       return opt<bool>(true);
                     }).is_some(), true);

    assert_eq(v.map([](const int &i) {
                      assert(false);
                      return i > 0;
                    }).is_some(), false);

    assert_eq(v.and_then([](const int &i) {
                           assert(false);
                           return opt<bool>(true);
                         }).is_some(), false);

    assert_eq(v.or_else([]() {
                          assert(true);
                          return opt<int>(-1);
                        }).is_some(), true);

    assert_eq(v.or_else([]() {
                          assert(true);
                          return opt<int>(-1);
                        }).unwrap(), -1);
  }

  {
    static const auto v = opt<int>(1);

    assert_eq(v.is_some(), true);
    assert_eq(v.is_none(), false);

    assert_eq(v.and_(9).is_some(), true);
    assert_eq(v.and_(9).unwrap(), 9);
    assert_eq(v.or_(9).is_some(), true);
    assert_eq(v.or_(9).unwrap(), 1);

    assert_eq(v.then([](const opt<int> &i) {
                       assert(true);
                       assert_eq(i.is_some(), true);
                       return 2;
                     }), 2);

    assert_eq(v.then([](const opt<int> &i) {
                       assert(true);
                       assert_eq(i.is_some(), true);
                       return opt<bool>();
                     }).is_some(), false);

    assert_eq(v.map([](int i) {
                      assert(true);
                      return i > 0;
                    }).unwrap(), true);

    assert_eq(v.and_then([](int i) {
                           assert(true);
                           return opt<bool>();
                         }).is_some(), false);

    assert_eq(v.and_then([](int i) {
                           assert(true);
                           return opt<int>(i + 1);
                         }).is_some(), true);

    assert_eq(v.and_then([](int i) {
                           assert(true);
                           assert_eq(i, 1);
                           return opt<int>(i + 1);
                         }).unwrap(), 2);

    assert_eq(v.or_else([]() {
                          assert(false);
                          return opt<int>(0);
                        }).is_some(), true);
  }
}
