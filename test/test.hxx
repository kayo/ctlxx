#pragma once

#include <iostream>

#define PRINT_RESET "\x1b[0m"
#define PRINT_DIM "\x1b[2m"
#define PRINT_RED "\x1b[31m"
#define PRINT_GREEN "\x1b[32m"
#define PRINT_YELLOW "\x1b[33m"

#define PRINT_WRAP(WHAT, WITH) WITH << (WHAT) << PRINT_RESET

#ifdef PASS
#define show_pass true
#else
#define show_pass false
#endif

#ifdef TEST
#define do_test true
#else
#define do_test false
#endif

#ifdef PLOT
#define do_plot true
#else
#define do_plot false
#endif

#define break_on_fail true

#define tests                                   \
  static unsigned tests_passed = 0;             \
  static unsigned tests_failed = 0;             \
  void tests_main();                            \
  int main(void) {                              \
    tests_main();                               \
    if (do_test) {                              \
      std::cout << "passed: " <<                \
        PRINT_WRAP(tests_passed, PRINT_GREEN)   \
                << " failed: " <<               \
        PRINT_WRAP(tests_failed, PRINT_RED)     \
                << std::endl;                   \
    }                                           \
    return tests_failed;                        \
  }                                             \
  void tests_main(void)

#define assert(expr) {                                            \
    if (expr) {                                                   \
      if (show_pass) {                                            \
        std::cout << "test " << PRINT_WRAP(#expr, PRINT_DIM)      \
                  << " " << PRINT_WRAP("PASS", PRINT_GREEN)       \
                  << std::endl;                                   \
      }                                                           \
      tests_passed ++;                                            \
    } else {                                                      \
      std::cout << "test " << PRINT_WRAP(#expr, PRINT_DIM)        \
                << " " << PRINT_WRAP("FAIL", PRINT_RED)           \
                << std::endl;                                     \
      tests_failed ++;                                            \
    }                                                             \
  }

#define assert_condition(actual, expected, condition) {         \
    auto actual_value = (actual);                               \
    auto expected_value = (expected);                           \
    auto error = actual_value - expected_value;                 \
    (void)error;                                                \
    if (condition) {                                            \
      if (show_pass) {                                          \
        std::cout << "test " << PRINT_WRAP(#actual, PRINT_DIM)  \
                  << " == " << PRINT_WRAP(#expected, PRINT_DIM) \
                  << " "                                        \
                  << PRINT_WRAP("PASS", PRINT_GREEN)            \
                  << std::endl;                                 \
      }                                                         \
      tests_passed ++;                                          \
    } else {                                                    \
      std::cout << "test " << PRINT_WRAP(#actual, PRINT_DIM)    \
                << " == " << PRINT_WRAP(#expected, PRINT_DIM)   \
                << " "                                          \
                << PRINT_WRAP("FAIL", PRINT_RED) <<             \
        std::endl << "  file: " <<                              \
        PRINT_WRAP(__FILE__, PRINT_YELLOW) <<                   \
        " line: " << PRINT_WRAP(__LINE__, PRINT_YELLOW) <<      \
        std::endl << "  actual: " <<                            \
        PRINT_WRAP(actual_value, PRINT_RED) << std::endl <<     \
        "  expected: " <<                                       \
        PRINT_WRAP(expected_value, PRINT_GREEN) << std::endl;   \
      tests_failed ++;                                          \
    }                                                           \
  }

#define assert_eq(actual_, expected_) assert_condition(actual_, expected_, actual_value == expected_value)
#define assert_eqe(actual_, expected_) assert_condition(actual_, expected_, error > -EPS && error < EPS)
