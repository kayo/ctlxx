#include <type/fix>
#include <filt/fir>

#include "test.hxx"

tests {
  {
    static const fir::param<int, 3> param = {9, 1, 7, 4};
    static fir::state<int, 3> state;

    const auto fir_apply = fir::apply<int, int, 3, int>;

    assert_eq(fir_apply(state, param, 0), 0);
    assert_eq(fir_apply(state, param, 1), 9);
    assert_eq(fir_apply(state, param, 0), 1);
    assert_eq(fir_apply(state, param, 0), 7);
    assert_eq(fir_apply(state, param, 0), 4);
    assert_eq(fir_apply(state, param, 0), 0);

    assert_eq(fir_apply(state, param, -1), -9);
    assert_eq(fir_apply(state, param, 1), 8);
    assert_eq(fir_apply(state, param, 3), 21);
    assert_eq(fir_apply(state, param, -7), -57);
    assert_eq(fir_apply(state, param, -7), -45);
    assert_eq(fir_apply(state, param, 10), 46);
  }

  {
    static const auto param = fir::param<float, 6>::from_window<float, float, float>(fir::dirichlet<float, 6>(), sinf, 10e3, 1e3, 10);

#define EPS 1e-6

    assert_eqe(param[0], 0.40309012577590075);
    assert_eqe(param[1], 0.1261653867650495);
    assert_eqe(param[2], 0.11986733923298123);
    assert_eqe(param[3], 0.1097897828321418);
    assert_eqe(param[4], 0.09653011352352994);
    assert_eqe(param[5], 0.08086441610927253);
    assert_eqe(param[6], 0.06369283576112421);

    //static fir::state<float, 6> state;
  }

#ifndef ORDER
#define ORDER 6
#endif

#ifndef F_BASE
#define F_BASE 10e6
#endif

#ifndef F_PASS
#define F_PASS 1e6
#endif

#ifndef F_CUT
#define F_CUT 10
#endif

#define PLOT_WINF(win, sin) {                   \
    static const auto param =                   \
      fir::param<float, ORDER>                  \
      ::from_window<float, float, float>        \
      (win, sin, F_BASE, F_PASS, F_CUT);        \
                                                \
    for (len_t i = 0; i <= ORDER; i++) {        \
      std::cout << param[i] << ' ';             \
    }                                           \
    std::cout << std::endl;                     \
  }

#ifdef RECTANGULAR
  PLOT_WINF((fir::rectangular<float, ORDER>()), sinf)
#endif

#ifdef BARTLETT
  PLOT_WINF((fir::bartlett<float, ORDER>()), sinf)
#endif

#ifdef PARZEN
  PLOT_WINF((fir::parzen<float, ORDER>()), sinf)
#endif
}
