# -*- mode: octave -*-

data = get_data("dqz", "DELTA_TIME", 0.0001, "LIMIT_TIME", 1/50, "PHASES", 3);

t = data(:,1);
a = data(:,2);
b = data(:,3);
d = data(:,4);
q = data(:,5);
ia = data(:,6);
ib = data(:,7);

subplot(3,1,1);
plot(t, a, "-;α;",
     t, b, "-;β;");
xlabel("t, S");
ylabel("U, V");
title("Source signal (Stationary frame)");

subplot(3,1,2);
plot(t, d, "-;d;",
     t, q, "-;q;");
xlabel("t, S");
ylabel("U, V");
title("Direct Park transformation (Rotating frame)");

subplot(3,1,3);
plot(t, ia, "-;α;",
     t, ib, "-;β;");
xlabel("t, S");
ylabel("U, V");
title("Inverted Park transformation (stationary frame)");
