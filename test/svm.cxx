#include <math/trig>
#include <trans/svm>
#include "test.hxx"

#ifndef DELTA_TIME
#define DELTA_TIME 0.001
#endif

#ifndef LIMIT_TIME
#define LIMIT_TIME 1.0
#endif

#ifndef PHASES
#define PHASES 3
#endif

tests {
  {
    auto param = osc::hpi::from_frequency<float>(50.0, DELTA_TIME);
    auto state = svm::state<float>();
    vec<len_t(PHASES), float> abc;

    for (float t = 0.0; t < LIMIT_TIME + DELTA_TIME; t += DELTA_TIME) {
      svm::hpi::apply(state, param, hpi::sin3<float, float, float>, abc);
#ifdef PLOT
      std::cout << t;
      for (int p = 0; p < len_t(PHASES); p++) {
        std::cout << ' ' << abc[p];
      }
      std::cout << std::endl;
#endif
    }
  }
}
