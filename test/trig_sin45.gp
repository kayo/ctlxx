#!/usr/bin/env gnuplot

set terminal x11 enhanced persist raise ctrlq replotonresize
set multiplot layout 2,1
set xrange [-4:4]
set xlabel 'radian'
set ylabel 'sine'
plot '< ./test.sh trig.cxx SIN4 PLOT' using 1:2 with steps title 'sin4', \
     '< ./test.sh trig.cxx SIN5 PLOT' using 1:2 with steps title 'sin5', \
     '< ./test.sh trig.cxx SIN4F PLOT' using 1:2 with steps title 'sin4f', \
     '< ./test.sh trig.cxx SIN5F PLOT' using 1:2 with steps title 'sin5f', \
     '< ./test.sh trig.cxx SIN4 PLOT' using 1:3 with steps title 'sinf'
set xrange [-4:4]
set xlabel 'radian'
set ylabel 'error'
plot '< ./test.sh trig.cxx SIN4 PLOT' using 1:($2-$3) with steps title 'sin4 error', \
     '< ./test.sh trig.cxx SIN5 PLOT' using 1:($2-$3) with steps title 'sin5 error', \
     '< ./test.sh trig.cxx SIN4F PLOT' using 1:($2-$3) with steps title 'sin4f error', \
     '< ./test.sh trig.cxx SIN5F PLOT' using 1:($2-$3) with steps title 'sin5f error'
#pause mouse key close
