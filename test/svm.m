# -*- mode: octave -*-

data = get_data("swm", "DELTA_TIME", 0.0001, "LIMIT_TIME", 1/50);

t = data(:,1);
a1 = data(:,2);
b1 = data(:,3);
c1 = data(:,4);
v1 = max(a1, max(b1, c1)) .- min(a1, min(b1, c1));
p1 = v1 .^ 2;

data = get_data("svm", "DELTA_TIME", 0.0001, "LIMIT_TIME", 1/50);

a2 = data(:,2);
b2 = data(:,3);
c2 = data(:,4);
v2 = max(a2, max(b2, c2)) .- min(a2, min(b2, c2));
p2 = v2 .^ 2;

subplot(3,1,1);
plot(t, a2, "-;A;",
     t, b2, "-;B;",
     t, c2, "-;C;");
xlabel("t, S");
ylabel("U, V");
title("3-phase space-vector modulation");

subplot(3,1,2);
plot(t, a1, "-;A;",
     t, b1, "-;B;",
     t, c1, "-;C;");
xlabel("t, S");
ylabel("U, V");
title("Simple 3-phase sine-wave modulation");

subplot(3,1,3);
plot(t, p2, "-;SVM;",
     t, p1, "-;SINE;");
xlabel("t, S");
ylabel("P, W");
title("Amount power of machine");
