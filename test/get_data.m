# -*- mode: octave -*-

function data = get_data(name, varargin)
  cmd = sprintf("./test.sh %s.cxx PLOT", name);
  i = 1;
  while i <= length(varargin)
    if ischar(varargin{i})
      cmd = sprintf("%s %s", cmd, varargin{i});
      i++;
      if i <= length(varargin) && isnumeric(varargin{i})
        cmd = sprintf("%s=%f", cmd, varargin{i});
        i++;
      endif
    else
      i++;
    endif
  endwhile
  [~, data] = system(cmd);
  data = str2num(data);
endfunction
