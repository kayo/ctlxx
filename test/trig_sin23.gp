#!/usr/bin/env gnuplot

set terminal x11 enhanced persist raise ctrlq replotonresize
set multiplot layout 2,1
set xrange [-4:4]
set xlabel 'radian'
set ylabel 'sine'
plot '< ./test.sh trig.cxx SIN2 PLOT' using 1:2 with steps title 'sin2', \
     '< ./test.sh trig.cxx SIN3 PLOT' using 1:2 with steps title 'sin3', \
     '< ./test.sh trig.cxx SIN2F PLOT' using 1:2 with steps title 'sin2f', \
     '< ./test.sh trig.cxx SIN3F PLOT' using 1:2 with steps title 'sin3f', \
     '< ./test.sh trig.cxx SIN3 PLOT' using 1:3 with steps title 'sinf'
set xrange [-4:4]
set xlabel 'radian'
set ylabel 'error'
plot '< ./test.sh trig.cxx SIN2 PLOT' using 1:($2-$3) with steps title 'sin2 error', \
     '< ./test.sh trig.cxx SIN3 PLOT' using 1:($2-$3) with steps title 'sin3 error', \
     '< ./test.sh trig.cxx SIN2F PLOT' using 1:($2-$3) with steps title 'sin2f error', \
     '< ./test.sh trig.cxx SIN3F PLOT' using 1:($2-$3) with steps title 'sin3f error'
#pause mouse key close
