# -*- mode: octave -*-

dt = 0.1;
tend = 400.0;

data = get_data("pid", "CONST_POWER", 26.0, "LIMIT_TIME", tend, "DELTA_TIME", dt);

t = data(:,1);
Tcp = data(:,2);
Pcp = data(:,3);

data = get_data("pid", "PID_TARGET", 240.0, "LIMIT_TIME", tend, "DELTA_TIME", dt, "PID_P", "PID_I", "PID_D");

Tpid = data(:,2);
Ppid = data(:,3);
Tpid_target = data(:,4);
Tpid_error = Tpid_target - Tpid;

subplot(1,2,1);
plot(t, Tcp, "-;Tcp;",
     t, Tpid, "-;Tpid;");
xlabel("t");
ylabel("T, *C");
title("Constant power vs PID controlled heating");

subplot(2,2,2);
plot(t, Pcp, "-;Pcp;",
     t, Ppid, '-;Ppid;');
xlabel("t");
ylabel("P, W");

subplot(2,2,4);
plot(t, Tpid_error, '-;Tpid error;');
xlabel("t");
ylabel("T error, *C");
