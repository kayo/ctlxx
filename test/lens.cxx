#include <util/lens>
#include "test.hxx"

struct S {
  int a;
  const char *b;
  unsigned char c;
};

tests {
  S s = { 1, "abc", 0x20 };

  // self
  assert_eq(lens::self(s).a, s.a);

  // field
  const auto pick_S_a = lens::field<S>(&S::a);
  const auto pick_S_b = lens::field<S>(&S::b);
  const auto pick_S_c = lens::field<S>(&S::c);

  assert_eq(pick_S_a(s), 1);
  assert_eq(pick_S_b(s), s.b);
  assert_eq(pick_S_c(s), s.c);

  // swap
  S s2 = {2, "def", 0x02};

  const auto swap_S_a = lens::swap<S>(pick_S_a);

  swap_S_a(s, s2);
  assert_eq(s.a, 2);
  assert_eq(s2.a, 1);
}
