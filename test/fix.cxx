#include "test.hxx"
#include <type/fix>

tests {
  using bfix1z0 = bfix<1,0>;
  using bfix7n3 = bfix<7,-3>;
  using bfix9n7 = bfix<9,-7>;
  using bfix12n7 = bfix<12,-7>;
  using bfix5p2 = bfix<5,2>;

  assert_eq(-_fix::pow_n(2, 7 - 1), -64);
  assert_eq(_fix::pow_n(2, 7 - 1) - 1, 63);
  assert_eq(-_fix::pow_n(2, 8 - 1), -128);
  assert_eq(_fix::pow_n(2, 8 - 1) - 1, 127);
  assert_eq(-_fix::pow_n(2, 32 - 1), -2147483648);
  assert_eq(_fix::pow_n(2, 32 - 1) - 1, 2147483647);
  assert_eq(-_fix::pow_n(2, 64 - 1), -9223372036854775807L - 1);
  assert_eq(_fix::pow_n(2, 64 - 1) - 1, 9223372036854775807L);

  assert_eq(bfix1z0::bits, 1);
  assert_eq(bfix1z0::base, 2);
  assert_eq(bfix1z0::exp, 0);

  assert_eq(bfix1z0::min().raw, -1);
  assert_eq(bfix1z0::max().raw, 0);

  assert_eq(float(bfix1z0::min()), -1.0);
  assert_eq(float(bfix1z0::max()), 0.0);

  assert_eq(bfix7n3::bits, 7);
  assert_eq(bfix7n3::base, 2);
  assert_eq(bfix7n3::exp, -3);

  assert_eq(bfix7n3::min().raw, -64);
  assert_eq(bfix7n3::max().raw, 63);
  assert_eq(bfix7n3::pre().raw, 1);

  assert_eq(float(bfix7n3::min()), -8.0);
  assert_eq(float(bfix7n3::max()), 7.875);
  assert_eq(float(bfix7n3::pre()), 0.125);

  assert_eq(float(bfix9n7::min()), -2.0);
  assert_eq(float(bfix9n7::max()), 1.9921875);
  assert_eq(float(bfix9n7::pre()), 0.0078125);

  assert_eq(float(bfix5p2::min()), -64.0);
  assert_eq(float(bfix5p2::max()), 60.0);
  assert_eq(float(bfix5p2::pre()), 4);

  bfix1z0 vbfix1z0;
  bfix7n3 vbfix7n3;
  bfix9n7 vbfix9n7;
  bfix5p2 vbfix5p2;

  assert_eq((vbfix1z0 + vbfix1z0).bits, 2);
  assert_eq((vbfix1z0 + vbfix1z0).exp, 0);

  assert_eq((vbfix7n3 + vbfix7n3).bits, 8);
  assert_eq((vbfix7n3 + vbfix7n3).exp, -3);

  assert_eq((vbfix7n3 + vbfix9n7).bits, 12);
  assert_eq((vbfix7n3 + vbfix9n7).exp, -7);

  assert_eq(bfix1z0(0).raw, 0);
  assert_eq(bfix1z0(1).raw, 1);

  assert_eq(bfix5p2(0).raw, 0);
  assert_eq(bfix5p2(1).raw, 0);
  assert_eq(bfix5p2(2).raw, 0);
  assert_eq(bfix5p2(4).raw, 1);
  assert_eq(bfix5p2(5).raw, 1);
  assert_eq(bfix5p2(7).raw, 1);
  assert_eq(bfix5p2(8).raw, 2);
  assert_eq(bfix5p2(0.0).raw, 0);
  assert_eq(bfix5p2(1.0).raw, 0);
  assert_eq(bfix5p2(4.1).raw, 1);

  assert_eq(bfix7n3(0.0).raw, 0);
  assert_eq(bfix7n3(0.1).raw, 0);
  assert_eq(bfix7n3(0.2).raw, 1);
  assert_eq(bfix7n3(0.3).raw, 2);
  assert_eq(bfix7n3(0.4).raw, 3);
  assert_eq(bfix7n3(0.5).raw, 4);
  assert_eq(int(bfix7n3(5.125).raw), 41);
  assert_eq(float(bfix7n3(5.125)), 5.125);

  assert_eq(bfix9n7(0.0).raw, 0);
  assert_eq(bfix9n7(-0.1).raw, -12);
  assert_eq(bfix9n7(0.1).raw, 12);
  assert_eq(bfix9n7(1.0).raw, 128);
  assert_eq(int(bfix9n7(1.25).raw), 160);
  assert_eq(int(bfix9n7(-1.25).raw), -160);
  assert_eq(float(bfix9n7(1.25)), 1.25);

  assert_eq(float(bfix9n7(0.1)), 0.09375);
  assert_eq(float(bfix9n7(1.5)), 1.5);
  assert_eq(float(bfix9n7(-1.25)), -1.25);

  assert_eq((int)bfix7n3(-5.125).raw, -41);
  assert_eq(bfix9n7(bfix7n3(-5.125)).raw, -656);

  //assert_eq(-10.25 + 5.125, -5.125);
  assert_eq(bfix12n7(-5.125).raw, -656);

  assert_eq(bfix9n7(-1.25) + bfix7n3(5.125), bfix12n7(-1.25 + 5.125));

  assert_eq(int(bfix9n7(-1.25).integral().raw), -1);
  assert_eq(int(bfix9n7(-1.25).fraction().raw), -32);
  assert_eq(bfix9n7(-1.25).integral(), (bfix<2,0>(-1)));
  assert_eq(bfix9n7(-1.25).fraction(), (bfix<7,-7>(-0.25)));

  assert_eq(bfix5p2(-15.0).integral(), (bfix<5,2>(-15.0)));
  assert_eq(bfix5p2(-15.0).fraction(), (bfix<0,0>(0.0)));

  assert_eq(int((bfix9n7(1.25) * bfix7n3(5.125)).raw), 6560);
  assert_eq(float(bfix9n7(1.25) * bfix7n3(5.125)), 6.40625);
  assert_eq(bfix9n7(1.25) * bfix7n3(5.125), (bfix<16,-10>(6.40625)));

  assert_eq(int((bfix9n7(1.25) * bfix7n3(-5.125)).raw), -6560);
  assert_eq(float(bfix9n7(1.25) * bfix7n3(-5.125)), -6.40625);
  assert_eq(bfix9n7(1.25) * bfix7n3(-5.125), (bfix<16,-10>(-6.40625)));

  assert_eq((bfix<16,-10>(-6.40625) / bfix<7,-3>(-5.125)), (bfix<9,-7>(1.25)));
  assert_eq((bfix<16,-10>(-6.40625) / bfix<9,-7>(1.25)), (bfix<7,-3>(-5.125)));

  assert_eq((bfix<16,-12>(10.5) % bfix<16,-12>(1)), (bfix<16,-12>(0.5)));
  assert_eq((bfix<16,-12>(-1.25) % bfix<10,-8>(1.2)), (bfix<10,-8>(-0.051)));
}
