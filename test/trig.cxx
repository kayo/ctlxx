#include <cmath>
#include <type/fix>
#include <math/trig>

#include "test.hxx"

#define assert_eqv(func, arg, expected)         \
  assert_eqe(double(actual), double(expected))

#define test_sin(sin) {                               \
    assert_eqv(sin(0.0), 0.0);                        \
    assert_eqv(sin(Pi/6.0), 1.0/2.0);                 \
    assert_eqv(sin(Pi/4.0), sqrt(2.0)/2.0);           \
    assert_eqv(sin(Pi/3.0), sqrt(3.0)/2.0);           \
    assert_eqv(sin(Pi/2.0), 1.0);                     \
                                                      \
    assert_eqv(sin(-Pi/6.0), -1.0/2.0);               \
    assert_eqv(sin(-Pi/4.0), -sqrt(2.0)/2.0);         \
    assert_eqv(sin(-Pi/3.0), -sqrt(3.0)/2.0);         \
    assert_eqv(sin(-Pi/2.0), -1.0);                   \
                                                      \
    assert_eqv(sin(2.0*Pi/3.0), sqrt(3.0)/2.0);       \
    assert_eqv(sin(3.0*Pi/4.0), sqrt(2.0)/2.0);       \
    assert_eqv(sin(5.0*Pi/6.0), 1.0/2.0);             \
    assert_eqv(sin(Pi), 0.0);                         \
                                                      \
    assert_eqv(sin(7.0*Pi/6.0), -1.0/2.0);            \
    assert_eqv(sin(5.0*Pi/4.0), -sqrt(2.0)/2.0);      \
    assert_eqv(sin(4.0*Pi/3.0), -sqrt(3.0)/2.0);      \
    assert_eqv(sin(3.0*Pi/2.0), -1.0);                \
                                                      \
    assert_eqv(sin(5.0*Pi/3.0), -sqrt(3.0)/2.0);      \
    assert_eqv(sin(7.0*Pi/4.0), -sqrt(2.0)/2.0);      \
    assert_eqv(sin(11.0*Pi/6.0), -1.0/2.0);           \
    assert_eqv(sin(2.0*Pi), 0.0);                     \
                                                      \
    assert_eqv(sin(2.0*Pi+Pi/4.0), sqrt(2.0)/2.0);    \
    assert_eqv(sin(-2.0*Pi-Pi/4.0), -sqrt(2.0)/2.0);  \
  }

#define RUN_FN1(from, to, step, fn, ref_fn, crit_err) {                 \
    double max_err = 0;                                                 \
    for (double x = from; x <= to; x += step) {                         \
      double actual = fn(x);                                            \
      double expected = ref_fn(x);                                      \
      double error = actual - expected;                                 \
      if (do_plot) {                                                    \
        std::cout << x << " " << double(actual)                         \
                  << " " << double(expected) << std::endl;              \
      }                                                                 \
      if (do_test) {                                                    \
        if (fabs(error) > max_err) {                                    \
          max_err = fabs(error);                                        \
        }                                                               \
        if (error > -crit_err && error < crit_err) {                    \
          tests_passed ++;                                              \
        } else {                                                        \
          std::cout << #fn << "(" << x << ") == "                       \
                    << double(actual)                                   \
                    << " but " << #ref_fn << "(" << x << ") == "        \
                    << double(expected) << std::endl                    \
                    << "error: " << double(error) << std::endl;         \
          tests_failed ++;                                              \
          if (break_on_fail) {                                          \
            break;                                                      \
          }                                                             \
        }                                                               \
      }                                                                 \
    }                                                                   \
    if (do_test && !tests_failed) {                                     \
      std::cout << "max error: " << max_err << std::endl;               \
    }                                                                   \
  }

#ifdef TEST
#define SIN2
#define SIN2F
#define SIN3
#define SIN3F
#define SIN4
#define SIN4F
#define SIN5
#define SIN5F
#endif

tests {
#ifdef SIN2
  RUN_FN1(-10.0, 10.0, 0.01, (sin2<float, float, float>), sinf, 0.057);
#endif

#ifdef SIN2F
  RUN_FN1(-10.0, 10.0, 0.01, (sin2<bfix<16,-14>, bfix<30,-24>, bfix<16,-12>>), sinf, 0.057);
#endif

#ifdef SIN3
  RUN_FN1(-10.0, 10.0, 0.01, (sin3<float, float, float>), sinf, 0.021);
#endif

#ifdef SIN3F
  RUN_FN1(-10.0, 10.0, 0.01, (sin3<bfix<16,-14>, bfix<30,-24>, bfix<16,-12>>), sinf, 0.021);
#endif

#ifdef SIN4
  RUN_FN1(-10.0, 10.0, 0.01, (sin4<float, float, float>), sinf, 0.003);
#endif

#ifdef SIN4F
  RUN_FN1(-10.0, 10.0, 0.01, (sin4<bfix<16,-15>, bfix<30,-24>, bfix<16,-12>>), sinf, 0.003);
#endif

#ifdef SIN5
  RUN_FN1(-10.0, 10.0, 0.01, (sin5<float, float, float>), sinf, 0.0002);
#endif

#ifdef SIN5F
  RUN_FN1(-10.0, 10.0, 0.01, (sin5<bfix<16,-15>, bfix<30,-24>, bfix<16,-12>>), sinf, 0.0005);
#endif
}
