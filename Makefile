all: doc test

doc:
	@doxygen

test:
	@make -C test

.PHONY: all doc test
